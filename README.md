# Dashboard Admin System API

### PLEASE COMPLETELY READ THESE INFORMATION FIRST!!

---

## Main Tech stacks

    * MySql as main database engine
    * NodeJS as server runner
    * Express as server framework
    * Sequelize as database ORM

## Getting Started

Getting up and running is as easy as 1, 2, 3.

1.  Make sure you have [NodeJS](https://nodejs.org/) and [npm](https://www.npmjs.com/) installed.
    Required version:
    ```
    "node": ">= 9.11.2"
    ```
2.  Install your dependencies

    ```
    cd path/to/fooder-api; npm install
    ```

3.  Configure things

    #### Create .env file in root directory of the project

    Copy .env.example file to new file named as .env then setup variable value following your machine.

4.  Migrate database using sequelize
    Run these commands in order

    ### If you have sequelize installed global

    ```
    sequelize db:create
    ```

    ```
    sequelize db:migrate
    ```

    ### If you have been not installing sequelize global

    ```
    node_modules/.bin/sequelize db:create
    ```

    ```
    node_modules/.bin/sequelize db:migrate
    ```

5.  OPTIONAL: You can run seeder to have dummy data in the table needed, for detailed information about the data like user password, etc. Look at src/database/seeders in the name dummy-data.js file. Just then run the command.

    ### If you have sequelize installed global

    ```
    sequelize db:seed:all
    ```

    ### If you have been not installing sequelize global

    ```
    node_modules/.bin/sequelize db:seed:all
    ```

    ### Alternative way, you can use bash seed.sh

6.  Start your app

    ```
    npm start
    ```

## License

Copyright (c) 2018
