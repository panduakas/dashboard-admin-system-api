const express = require('express');
const productsControllers = require('../../controllers/products');
const auth = require('../../helpers/v1/auth').auth;
const route = express.Router();

route.post('/', auth, productsControllers.createProducts); /// add new Products to database 
route.get('/:categoryId', auth, productsControllers.getAllProducts); // get all Products
route.patch('/:productsId', auth, productsControllers.updateProducts); // update data Products
route.delete('/:productsId', auth, productsControllers.deleteProducts); // delete Products from database

module.exports = route;
