const express = require('express');
const users = require('./users');
const products = require('./products');

// Declare API Route and API Version
const v1 = express.Router();

// v1.use('/me', me);
v1.use('/users', users);
v1.use('/products', products);


module.exports = v1;
