const express = require('express');
const usersControllers = require('../../controllers/users');
const auth = require('../../helpers/v1/auth').auth;
const route = express.Router();

route.post('/login', usersControllers.login); // login end point
route.post('/register', usersControllers.register); // register end point
route.get('/', auth, usersControllers.getAllAdmin); // show all admin registered
route.delete('/:usersId', auth, usersControllers.deleteUsers); // delete user

module.exports = route;
