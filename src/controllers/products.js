const {
  response
} = require('../helpers/v1/response');
const {
  products
} = require('../services/v1');

const createProducts = async (req, res) => {
  const {
    name,
    code,
    category
  } = req.body;
  try {
    const addNewProducts = await products.addProducts(name, code, category);
    if (!addNewProducts) {
      throw new Error('Failed to add a new product');
    } else {
      return res.status(200).json(response(true, 'Successfully to add a new product', addNewProducts));
    }
  } catch (error) {
    res.status(400).json(response(false, error.message));
  }
};

const getAllProducts = async (req, res) => {
  try {
    const showProduct = await products.findProductsByCategoryId(req.params.categoryId);
    if (!showProduct) {
      throw new Error('Failed to show product list');
    } else {
      res.status(200).json(response(true, 'Product list successfully retrieved', showProduct));
    }
  } catch (error) {
    res.status(400).json(response(false, error.message));
  }
};

const updateProducts = async (req, res) => {
  const {
    name,
    code,
    category
  } = req.body;
  try {
    const productsUpdate = await products.updateProductsByProductId(req.params.productsId, name, code, category);

    if (!productsUpdate) {
      throw new Error('Failed update product');
    } else {
      res.status(200).json(response(true, 'Success update product', productsUpdate));
    }

  } catch (error) {
    res.status(400).json(response(false, error.message));
  }
};

const deleteProducts = async (req, res) => {
  try {
    const currentData = await products.findProductById(req.params.productsId);
    const destroyProducts = await products.deleteProducts(req.params.productsId);
    if (!destroyProducts) {
      throw new Error('Failed to delete product');
    } else {
      return res.status(200).json(response(true, 'Successfully delete product', currentData));
    }
  } catch (error) {
    res.status(400).json(response(false, error.message));
  }
};

module.exports = {
  createProducts,
  getAllProducts,
  updateProducts,
  deleteProducts
};
