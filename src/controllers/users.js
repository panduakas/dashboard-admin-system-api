const {
  response
} = require('../helpers/v1/response');
const {
  users
} = require('../services/v1');
require('dotenv').config();
const jwt = require('jsonwebtoken');
const passport = require('passport');
const bcrypt = require('bcrypt');

const login = (req, res, next) => {
  passport.authenticate(
    'local', {
      session: false
    },
    async (err, user, info) => {
      try {
        if (err) {
          return res.status(400).next(err);
        }
        if (!user) {
          return res.status(400).json({
            message: info.message
          });
        }
        const data = {
          email: user.email,
          createdAt: new Date()
        };
        const token = jwt.sign(data, process.env.JWT_SECRET, {
          expiresIn: 86400 * 7
        });
        const included = {
          token: token
        };
        if (!token) {
          throw new Error('Failed to create token');
        } else {
          return res
            .status(200)
            .json(response(true, 'User signed in', user, included));
        }
      } catch (error) {
        return res.status(400).json(response(false, error.message));
      }
    }
  )(req, res, next);
};

const register = async (req, res) => {
  const {
    email,
    phone,
    password,
    password2
  } = req.body;
  try {
    const checkUser = await users.findUser(email, phone);
    if (checkUser) {
      throw new Error('Email or Phone already exist!');
    }
    if (password !== password2) {
      throw new Error('Password comfirmation does not match');
    }
    const newPassword = await bcrypt.hash(password, 10);
    const userCreate = await users.createUser(email, phone, newPassword);
    if (!userCreate) {
      throw new Error('Failed to create a new customer');
    } else {
      res.status(200).json(response(true, 'Registration Success!', userCreate));
    }
  } catch (error) {
    return res.status(400).json(response(false, error.message));
  }
};

const getAllAdmin = async (req, res) => {
  try {
    const showAdmin = await users.allAdmin();
    if (!showAdmin) {
      throw new Error('Failed to show all admin.');
    } else {
      res.status(200).json(response(true, 'All admin successfully retrieved.', showAdmin));
    }
  } catch (error) {
    res.status(400).json(response(false, error.message));
  }
};

const deleteUsers = async (req, res) => {
  try {
    const dataUser = await users.findUserById(req.params.usersId);
    const destroyUser = await users.userDelete(req.params.usersId);
    if (!destroyUser) {
      throw new Error('Failed to delete user');
    } else {
      res.status(200).json(response(true, 'Successfully delete user', dataUser));
    }
  } catch (error) {
    res.status(400).json(response(false, error.message));
  }
};

module.exports = {
  login,
  register,
  getAllAdmin,
  deleteUsers
};
