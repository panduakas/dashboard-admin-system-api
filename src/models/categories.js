'use strict';
module.exports = (sequelize, DataTypes) => {
  var categories = sequelize.define('categories', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    name: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    description: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    timestamp: true,
    underscored: true
  });
  categories.associate = function (models) {
    // associations can be defined here
    categories.hasMany(models.product_categories, {
      foreignKey: 'category_id',
      onDelete: 'cascade',
      hooks: true
    });
  };
  return categories;
};
