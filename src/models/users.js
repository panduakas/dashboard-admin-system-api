'use strict';
module.exports = (sequelize, DataTypes) => {
  var users = sequelize.define('users', {
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        isEmail: {
          msg: 'Invalid email!'
        },
        notEmpty: {
          msg: 'Email is required!'
        }
      }
    },
    phone: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        len: {
          args: [10, 12],
          msg: 'Phone number is invalid! (10 - 12) characters'
        },
        not: {
          args: ['[a-z]', 'i'],
          msg: 'Phone number is invalid!'
        }
      }
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    timestamp: true,
    underscored: true
  });
  users.associate = function (models) {
    // associations can be defined here
  };
  return users;
};
