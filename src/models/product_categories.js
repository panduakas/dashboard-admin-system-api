'use strict';
module.exports = (sequelize, DataTypes) => {
  var product_categories = sequelize.define('product_categories', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    product_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      foreignKey: true,
      references: {
        model: 'products',
        key: 'id'
      }
    },
    category_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      foreignKey: true,
      references: {
        model: 'categories',
        key: 'id'
      }
    }
  }, {
    timestamp: true,
    underscored: true
  });
  product_categories.associate = function(models) {
    // associations can be defined here
    product_categories.belongsTo(models.products, {
      foreignKey: 'product_id'
    });
    product_categories.belongsTo(models.categories, {
      foreignKey: 'category_id'
    });
  };
  return product_categories;
};