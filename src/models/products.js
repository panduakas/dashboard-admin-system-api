'use strict';
module.exports = (sequelize, DataTypes) => {
  var products = sequelize.define('products', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    code: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    timestamp: true,
    underscored: true
  });
  products.associate = function(models) {
    // associations can be defined here
    products.hasMany(models.product_categories, {
      foreignKey: 'product_id',
      onDelete: 'cascade',
      hooks: true
    });
  };
  return products;
};