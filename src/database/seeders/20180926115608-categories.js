'use strict';
const faker = require('faker');
let dataSample = [];
let categoryCount = [
  'Helmets',
  'Jackets',
  'Gloves',
  'Shoes',
  'Backpacks'
];
for (let i = 0; i < 5; i++) {
  let contentData = {
    name: categoryCount[i],
    description: faker.lorem.sentence(),
    created_at: new Date(),
    updated_at: new Date()
  };
  dataSample.push(contentData);
}

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('categories', dataSample, {});
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('categories', null, {});
  }
};
