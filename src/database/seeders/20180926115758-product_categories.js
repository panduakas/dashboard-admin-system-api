'use strict';
const faker = require('faker');
let dataSample = [];
for (let i = 0; i < 15; i++) {
  let contentData = {
    product_id: faker.random.number({
      min: 1,
      max: 10
    }),
    category_id: faker.random.number({
      min: 1,
      max: 5
    }),
    created_at: new Date(),
    updated_at: new Date()
  };
  dataSample.push(contentData);
}

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('product_categories', dataSample, {});
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('product_categories', null, {});
  }
};
