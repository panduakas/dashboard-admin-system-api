'use strict';
const faker = require('faker');
function makeCode() {
  let text = '';
  const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

  for (var i = 0; i < 5; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  
  return text;
}
let dataSample = [];
for (let i = 0; i < 10; i++) {
  let contentData = {
    name: faker.name.firstName(),
    code: makeCode(),
    created_at: new Date(),
    updated_at: new Date()
  };
  dataSample.push(contentData);
}

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('products', dataSample, {});
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('products', null, {});
  }
};
