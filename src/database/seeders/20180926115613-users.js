'use strict';
require('dotenv').config();
const faker = require('faker');
const bcrypt = require('bcrypt');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    let dataSample = [];
    let password = await bcrypt.hash(
      process.env.BCRYPT_PASS,
      parseInt(process.env.BCRYPT_SALT)
    );
    for (let i = 1; i < 5; i++) {
      let contentData = {
        email: faker.internet.email(),
        phone: faker.phone.phoneNumber('0821########'),
        password: `${password}`,
        created_at: new Date(),
        updated_at: new Date()
      };
      dataSample.push(contentData);
    }

    let superAdmin = {
      email: 'panduakas@gmail.com',
      phone: '082119322388',
      password: password,
      created_at: new Date(),
      updated_at: new Date()
    };

    dataSample.push(superAdmin);
    return queryInterface.bulkInsert('users', dataSample, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('users', null, {});
  }
};
