const { products, product_categories } = require('../../models');

const productsService = {
  addProducts: async (name, code, category) => {
    const data = await products.create({
      name,
      code
    });
    if (category) {
      await Promise.all(category.map(async category_id => {
        await product_categories.create({
          product_id: data.id,
          category_id
        });
      }));
    }
    return {data, category};
  },
  updateProductsByProductId: async (product_id, name, code, category) => {
    const checkData = await products.findById(product_id);
    const data = await checkData.update({
      name,
      code
    });
    if (category) {
      await product_categories.destroy({
        where: {
          product_id
        }
      });
      await Promise.all(category.map(async category_id => {
        await product_categories.create({
          product_id,
          category_id
        });
      }));
    }
    return {data, category};
  },
  findProductById: async id => {
    const data = await products.findById(id);
    return data;
  },
  deleteProducts: async id => {
    await product_categories.destroy({
      where: {
        product_id: id
      }
    });
    const data = await products.destroy({
      where: {
        id
      }
    });
    return data;
  },
  findProductsByCategoryId: async id => {
    const data = await product_categories.findAll({
      where: {
        category_id: id
      },
      include: [{
        model: products
      }]
    });
    return data;
  }
};

module.exports = productsService;