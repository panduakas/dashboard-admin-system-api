const {
  users
} = require('../../models');
  
const user = {
  findUser: async (email, phone) => {
    const data = await users.findOne({
      where: {
        email,
        phone
      }
    });
    return data;
  },
  findUserAdmin: async (email, phone) => {
    const data = await users.findOne({
      where: {
        email,
        phone
      }
    });
    return data;
  },
  createUser: async ( email, phone, password) => {
    const data = await users.create({
      email,
      phone,
      password
    });
    return data;
  },
  allAdmin: async () => {
    const data = await users.findAll();
    return data;
  },
  findUserById: async (id) => {
    const data = await users.findById(id);
    return data;
  },
  userDelete: async (id) => {
    const data = await users.destroy({
      where: {
        id
      }
    });
    return data;
  }
};
  
module.exports = user;
  