require('module-alias/register');
const compress = require('compression');
const cors = require('cors');
const helmet = require('helmet');
const morgan = require('morgan');
const express = require('express');
const cookieParser = require('cookie-parser');
const passport = require('passport');
require('../config/passport');
const winston = require('../config/winston');
const routes = require('./routes');


const app = express();

app.use(passport.initialize());
app.use(cookieParser());
app.use(cors());
app.use(helmet());
app.use(compress());
app.use(morgan('combined', { stream: winston.stream }));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // add this line to include winston logging
  winston.error(
    `${err.status || 500} - ${err.message} - ${req.originalUrl} - ${
      req.method
    } - ${req.ip}`
  );

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.use('/api/v1', routes.v1);

module.exports = app;
