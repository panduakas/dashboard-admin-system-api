require('dotenv').config();
module.exports = {
  development: {
    username: process.env.DEV_USER,
    password: process.env.DEV_PASS,
    database: process.env.DEV_DATABASE,
    host: process.env.DEV_HOST,
    dialect: 'mysql',
    dialectOptions: {
      dateStrings: true,
      typeCast: true,
      timezone: 'Asia/Jakarta' //for reading from database
    },
    timezone: 'Asia/Jakarta'
  }
};
